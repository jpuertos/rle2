############################################
# Copyright Juan V. Puertos 2018           #
############################################

# Loads the message names of top level messages present in the given file stream
def get_toplevel_message_names(input_fs)
  message_names = []
  input_fs.each_line do |line|
    if line =~ /^message\s(\w+)/
      message_names << $1
    end
  end
  return message_names
end

# Loads all the top level message definitions of .proto files existing in 'dir'
# except for the files listed in 'exclude'
def load_message_names_from_dir(proto_path, exclude)
  message_names = []
  file_names = Dir["#{proto_path}/*.proto"] - exclude
  file_names.each do |file_name|
    fs = File.new(file_name, 'r')
    message_names << get_toplevel_message_names(fs)
    fs.close
  end
  return message_names.flatten!.sort!
end

# Generates the enumeration values from the message_names list into the output file stream
# provided. Enumeration values are written in uppercase and sorted.
def generate_enum_values(message_names, output_fs)
  seq = 0
  message_names.sort!
  message_names.each do |name|
    output_fs.puts("  #{name.upcase} = #{seq};")
    seq += 1
  end
end

#Generates a MessageIds.proto file containing the enumeration MessageId
def generate_message_ids_file(message_names, output_fs)
  output_fs.puts("syntax=\"proto2\";")
  output_fs.puts("package rle2.pb;")
  output_fs.puts("")
  output_fs.puts("// THIS IS GENERATED CODE BY 'messageids_generator.rb' DO NOT MODIFY!")
  output_fs.puts("enum message_id")
  output_fs.puts("{")
  generate_enum_values(message_names, output_fs)
  output_fs.puts("}")
end

# Generates the struct that holds all the possible messages
# used as a cache for parsing incomming messages.
#
def generate_messages_cache_struct_header(proto_path, exclude, message_names, struct_name, output_fs)
  output_fs.puts("#ifndef RLE2_MESSAGES_CACHE_HPP")
  output_fs.puts("#define RLE2_MESSAGES_CACHE_HPP")
  output_fs.puts("")  
  proto_file_names = Dir["#{proto_path}/*.proto"] - exclude
  proto_file_names.each do |file_name|
    file_name.sub!(".proto", ".pb.h")
    output_fs.puts("#include \"#{file_name}\"")
  end

  output_fs.puts("")
  output_fs.puts("namespace rle2")
  output_fs.puts("{")
  output_fs.puts("")
  output_fs.puts("struct #{struct_name}")
  output_fs.puts("{")

  message_names.each do |message|
    output_fs.puts("    rle2::pb::#{message} #{message};")    
  end

  output_fs.puts("};")
  output_fs.puts("")
  output_fs.puts("} // namespace")
  output_fs.puts("")
  output_fs.puts("#endif")
  output_fs.puts("")
end

# Generates the code of the unwrap method of class message_parser.
# Calls the ParseFromArray method of the appropriate message type
# depending on the message_id present at the wrapped message.
# Assumes that class name is the same as the header file name.
def generate_unwrap_implementation(message_names, class_name, output_fs)
  output_fs.puts("#include \"#{class_name}.hpp\"")
  output_fs.puts("")
  output_fs.puts("namespace rle2")
  output_fs.puts("{")
  output_fs.puts("")
  output_fs.puts("void #{class_name}::unwrap()")
  output_fs.puts("{")
  output_fs.puts("    switch (wrapped_message_.id())")
  output_fs.puts("    {")

  message_names.each do |message|
    output_fs.puts("        case rle2::pb::message_id::#{message.upcase}:")
    output_fs.puts("        {")
    output_fs.puts("            cache_.#{message}.ParseFromArray(wrapped_message_.msg().data(), wrapped_message_.msg().length());")
    output_fs.puts("            // TODO: Push to the message queue")
    output_fs.puts("            break;")
    output_fs.puts("        }")
  end

  output_fs.puts("        default:")
  output_fs.puts("            // TODO: Log error, should never happen, received message_id...")
  output_fs.puts("            break;")
  output_fs.puts("    }") # switch
  output_fs.puts("}") # unwrap()
  output_fs.puts("")
  output_fs.puts("} // namespace rle2")
  
end


#######################
# Script Main code

proto_path = ARGV[0]
message_ids_fname = "message_ids.proto"
messages_cache_fname = "messages_cache.hpp"
unwrap_implementation_fname = "message_parser_unwrap.cpp"
exclude_files = ["#{proto_path}/wrapper_message.proto", "#{proto_path}/#{message_ids_fname}"]

# Generate the message_ids.proto file
puts "Generating #{message_ids_fname}..."
message_ids_fs = File.new("#{proto_path}/#{message_ids_fname}", 'w')
message_names = load_message_names_from_dir(proto_path, exclude_files) # This is reused in subsequent generation of code
generate_message_ids_file(message_names, message_ids_fs)
message_ids_fs.close
puts "done."

# Generate the messages_cache struct header file
puts " Generating #{messages_cache_fname}..."
messages_chache_header_fs = File.new("#{proto_path}/../#{messages_cache_fname}", 'w')
generate_messages_cache_struct_header(proto_path, exclude_files, message_names, "messages_cache", messages_chache_header_fs)
messages_chache_header_fs.close
puts "done."

# Generate the implementation of the unwrap() method
puts " Generating #{unwrap_implementation_fname}..."
unwrap_implementation_fs = File.new("#{proto_path}/../#{unwrap_implementation_fname}", 'w')
generate_unwrap_implementation(message_names, "message_parser", unwrap_implementation_fs)
unwrap_implementation_fs.close
puts "done."
