1. Install Development packages:

  $ apt install build-essential ruby2.5 protobuf-compiler cmake libprotoc-dev libboost-all-dev libtcod-dev libnoise-dev

  NOTE: If problems found installing libboost-all-dev then try to install it using aptitude
  
2. Create a build dir under the rle2 folder go there and run cmake:

  $ mkdir build && cd build
  $ cmake ..

3. To generate the protobuf C++ code from the build folder run:

  $ make generate-protobuf

4. Everytime we create a new .proto file we need to update the CMakeLists.txt at stc/common/pb:
  - List the new file under PROTOBUF_FILES
  - List the new file .pb.h under LIBPROTOBUF_HEADERS
  - List the new file .pb.h under LIBPROTOBUF_SOURCES
  - Add Command to generate its C++ code: COMMAND ${PROTOC_COMPILER} ${CMAKE_CURRENT_SOURCE_DIR}/<file name>.proto --proto_path ${PROTOC_PROTOPATH} --cpp_out ${PROTOC_OUTPUTDIR}
