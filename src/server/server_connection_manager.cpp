#include "server/server_connection_manager.hpp"
#include "common/stream_parser.hpp"

namespace rle2 {
namespace net {

server_connection_manager::server_connection_manager()
{
}

void server_connection_manager::start(std::shared_ptr<connection> c)
{
    auto queue = std::make_shared<rle2::queue<rle2::pb::wrapper_message>>();
    c->set_read_handler(std::make_unique<stream_parser>(queue));
    connections_[c] = queue;
    c->start();
}

void server_connection_manager::stop(std::shared_ptr<connection> c)
{
    c->stop();
    connections_.erase(c);
}

void server_connection_manager::stop_all()
{
    for (auto c : connections_)
    {
        c.first->stop();
    }
    connections_.clear();
}

} // namespace net
} // namespace rle2
