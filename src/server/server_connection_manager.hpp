#ifndef RLE2_CONNECTION_MANAGER_HPP
#define RLE2_CONNECTION_MANAGER_HPP

#include "common/net/connection.hpp"
#include "common/net/connection_manager_if.hpp"
#include "common/queue.hpp"
#include "common/pb/wrapper_message.pb.h"

#include <map>

namespace rle2 {
namespace net {

/// Manages open connections so that they may be cleanly stopped when the server
/// needs to shut down.
class server_connection_manager : public connection_manager_if
{
public:
    using map = std::map<std::shared_ptr<connection>, std::shared_ptr<rle2::queue<rle2::pb::wrapper_message>>>;

    server_connection_manager(const server_connection_manager&) = delete;
    server_connection_manager& operator=(const server_connection_manager&) = delete;
    server_connection_manager();

    virtual void start(std::shared_ptr<connection> c) override;

    virtual void stop(std::shared_ptr<connection> c) override;

    virtual void stop_all() override;

    // TODO: Add method to stop  specific connection.

private:
    /// The managed connections.
    map connections_;
};

} // namespace net
} // namespace rle2

#endif // RLE2_CONNECTION_MANAGER_HPP
