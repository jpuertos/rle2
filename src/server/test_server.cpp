#include "common/net/server.hpp"
#include "server_connection_manager.hpp"

#include <iostream>
#include <string>

int main(int argc, char* argv[])
{
    try
    {
        // Check command line arguments.
        if (argc != 3)
        {
            std::cerr << "Usage: test-server <address> <port>\n";
            std::cerr << "  For IPv4, try:\n";
            std::cerr << "    receiver 0.0.0.0 80 .\n";
            std::cerr << "  For IPv6, try:\n";
            std::cerr << "    receiver 0::0 80 .\n";
            return 1;
        }

        auto io_service = std::make_shared<boost::asio::io_service>(1); // Why 1, what does it mean?
        auto connection_manager = std::make_shared<rle2::net::server_connection_manager>();
        // Initialise the server.
        rle2::net::server s(io_service, argv[1], argv[2], connection_manager);

        // Run the server until stopped.
        s.run();
    }
    catch (std::exception& e)
    {
        std::cerr << "exception: " << e.what() << "\n";
    }

    return 0;
}
