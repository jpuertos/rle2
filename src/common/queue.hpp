#ifndef RLE2_QUEUE_HPP
#define RLE2_QUEUE_HPP

#include <queue>
#include <mutex>
#include <vector>

namespace rle2
{

/**
 * @brief The queue class is a MT safe queue of
 * ValueType values. It can be used for passing messages
 * between threads.
 */

template <typename ValueType>
class queue
{
public:
    queue() = default;
    ~queue() = default;
    queue(const queue&) = delete;
    queue& operator=(const queue&) = delete;
    queue(queue&&) = delete;
    queue& operator=(queue&&) = delete;

    bool empty()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        return queue_.empty();
    }

    std::size_t size()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        return queue_.size();
    }

    const ValueType& front()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        return queue_.front();
    }

    void push(const ValueType& value)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        queue_.push(value);
    }

    void push(const std::vector<ValueType>& values)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        for (auto const & v : values)
        {
            queue_.push(v);
        }
    }

    void pop()
    {
        std::lock_guard<std::mutex> lock(mutex_);
        queue_.pop();
    }

private:
    std::mutex mutex_;
    std::queue<ValueType> queue_;

};

} // namespace rle2

#endif
