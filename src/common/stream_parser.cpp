#include "stream_parser.hpp"

namespace rle2 {

stream_parser::stream_parser(std::shared_ptr<rle2::queue<rle2::pb::wrapper_message>> queue)
    : cursor_(0),
      next_message_size_(),
      next_message_size_bytes_needed_(sizeof(message_size_t)),
      current_message_bytes_needed_(0),
      current_message_size_(0),
      partial_messages_(),
      received_messages_buffers_(),
      message_parser_(queue)
{
}

void stream_parser::parse(boost::asio::const_buffer buffer, const std::size_t size)
{
    if (collecting_message_size())
    {
        collect_message_size(buffer, size);
    }

    if (collecting_message())
    {
        collect_message(buffer, size);
    }

    if (!is_buffer_consumed(size))
    {
        parse(buffer, size);
    }

    message_parser_.parse(received_messages_buffers_);
    cursor_ = 0; // Rewind
}

void stream_parser::clear()
{
    received_messages_buffers_.clear();
    partial_messages_.get_completed().clear();
}

bool stream_parser::collect_message_size(boost::asio::const_buffer buffer, const std::size_t size)
{
    if (is_remaining_message_size_available(size)) // Either a whole message size element or the last fragment is available
    {
        if (next_message_size_bytes_needed_ == sizeof(message_size_t)) // A complete message size is needed and is available
        {
            current_message_size_ = *reinterpret_cast<const message_size_t*>(boost::asio::buffer_cast<const std::uint8_t*>(buffer) + cursor_);
            current_message_bytes_needed_ = current_message_size_;
            cursor_ += sizeof(message_size_t); // We need to update the cursor
        }
        else // We already have one fragment then we read the next (and last) fragment available
        {
            collect_element_part(buffer, size, next_message_size_bytes_needed_, next_message_size_);
            current_message_size_ = *reinterpret_cast<message_size_t*>(next_message_size_.data());
            current_message_bytes_needed_ = current_message_size_;
            next_message_size_.clear();
        }
        next_message_size_bytes_needed_ = 0;
        return true;
    }
    else // Fragment of next message size, we need to store the available bytes in our local buffer
    {
        collect_element_part(buffer, size, next_message_size_bytes_needed_, next_message_size_);
        return false;
    }
}

bool stream_parser::collect_message(boost::asio::const_buffer buffer, const std::size_t size)
{
    if (is_remaining_message_available(size)) // Either a whole message or the last fragment of current message is available
    {
        if (current_message_bytes_needed_ == current_message_size_) // The whole message is available
        {
            received_messages_buffers_.emplace_back(boost::asio::buffer(boost::asio::buffer_cast<const std::uint8_t*>(buffer) + cursor_,
                                                                        current_message_size_));
            cursor_ += current_message_size_;
            current_message_bytes_needed_ = 0;
        }
        else // It is the last fragment of an incomplete message
        {
            auto local_buffer = partial_messages_.get_active();
            collect_element_part(buffer, size, current_message_bytes_needed_, local_buffer);
            received_messages_buffers_.emplace_back(boost::asio::buffer(local_buffer.data(), current_message_size_));
            partial_messages_.flip(); // active one becomes completed in the reservoir
            // NOTE: We do not clear the local buffer (the completed element at the reservoir) because the content has not
            // still been used. After consumed then we need to free it.
        }
        next_message_size_bytes_needed_ = sizeof(message_size_t); // Need to indicate that a message size element is what comes next.
        return true;
    }
    else // Only a fragment of the current message is present
    {
        auto local_buffer = partial_messages_.get_active();
        collect_element_part(buffer, size, current_message_bytes_needed_, local_buffer);
        return false;
    }
}

std::size_t stream_parser::bytes_left_in_buffer(std::size_t size) const
{
    return size - cursor_;
}

bool stream_parser::collecting_message_size() const
{
    return next_message_size_bytes_needed_ > 0;
}

bool stream_parser::collecting_message() const
{
    return current_message_bytes_needed_ > 0;
}

bool stream_parser::is_remaining_message_size_available(std::size_t size) const // Are bytes needed to complete a message size available?
{
    return bytes_left_in_buffer(size) >= next_message_size_bytes_needed_;
}

bool stream_parser::is_remaining_message_available(std::size_t size) const
{
    return bytes_left_in_buffer(size) >= current_message_bytes_needed_;
}

bool stream_parser::is_buffer_consumed(std::size_t size) const
{
    return cursor_ == size;
}

template <typename T>
void stream_parser::collect_element_part(boost::asio::const_buffer buffer,
                                                const std::size_t size,
                                                message_size_t& bytes_needed,
                                                T& container)
{
    auto read_size = std::min(bytes_needed, static_cast<message_size_t>(bytes_left_in_buffer(size)));
    auto buffer_pos = boost::asio::buffer_cast<const std::uint8_t*>(buffer) + cursor_;
    container.insert(container.end(), buffer_pos, buffer_pos + read_size);
    bytes_needed -= read_size;
    cursor_ += read_size;
}

} // namespace rle2
