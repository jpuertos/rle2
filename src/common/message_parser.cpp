#include "message_parser.hpp"
#include "common/pb/wrapper_message.pb.h"

namespace rle2 {

message_parser::message_parser(std::shared_ptr<rle2::queue<rle2::pb::wrapper_message>> queue) : queue_(queue)
{

}

void message_parser::parse(const std::vector<boost::asio::const_buffer>& buffers)
{
    for (const auto& buffer : buffers)
    {
        if (wrapped_message_.ParseFromArray(boost::asio::buffer_cast<const void*>(buffer), boost::asio::buffer_size(buffer)))
        {
            queue_->push(wrapped_message_); // We push a copy of the wrapped message we just parsed.
        }
        else
        {
            // TODO: Log error, could not parse the message
        }
    }
}

} // namespace rle2
