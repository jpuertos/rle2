#ifndef RLE2_STREAM_PARSER_HPP
#define RLE2_STREAM_PARSER_HPP

#include "net/read_completion_handler_if.hpp"
#include "common/message_parser.hpp"

#include <vector>

namespace rle2 {

/**
 * @brief The stream_parser class parses a stream of messages. The message stream is a sequence of message size
 * followed by the message itself. This class implements the read_completion_handler_if interface and its method
 * parse gets called by 'connection' everytime that data has been read from the socket.
 *
 */
class stream_parser : public rle2::net::read_completion_handler_if
{
public:
    using message_size_t = std::uint32_t;

    /**
     * @brief The flip_flop_reservoir class
     *
     * Both containers may be empty at any given time (and initially)
     * Only one container can be complete, the active container is the other one then
     * Once a container becomes complete the other one becomes the active one
     * We need to clear a complete reservoir
     */
    class flip_flop_reservoir
    {
    public:
        flip_flop_reservoir() : partial_messages_(), active_(0), completed_(1) {}

        std::string& get_active()
        {
            return partial_messages_[active_];
        }

        std::string& get_completed()
        {
            return partial_messages_[completed_];
        }

        void flip() // Flipping only when the active storage becomes complete
        {
            active_ = (active_ + 1) % 2;
            completed_ = (completed_ + 1) % 2;
        }

        void clear_completed()
        {
            partial_messages_[completed_].clear();
        }

    private:
        std::array<std::string, 2> partial_messages_;
        std::size_t active_;
        std::size_t completed_;
    };

    stream_parser(std::shared_ptr<rle2::queue<rle2::pb::wrapper_message>> queue);
    stream_parser(const stream_parser&) = delete;
    stream_parser& operator=(const stream_parser) = delete;
    ~stream_parser() = default;
    stream_parser(stream_parser&&) = delete;
    stream_parser& operator=(stream_parser&&) = delete;

    virtual void parse(boost::asio::const_buffer buffer, const std::size_t size);

    // This can be called after the messages have been procesed and the buffers are not needed anymore.
    void clear();

private:
    std::size_t bytes_left_in_buffer(std::size_t size) const;
    bool collecting_message_size() const;
    bool collecting_message() const;
    bool is_remaining_message_size_available(std::size_t size) const;
    bool is_remaining_message_available(std::size_t size) const;
    bool is_buffer_consumed(std::size_t size) const;

    template <typename T>
    void collect_element_part(boost::asio::const_buffer buffer, const std::size_t size, message_size_t& bytes_needed, T& container);

    bool collect_message_size(boost::asio::const_buffer buffer, const std::size_t size);
    bool collect_message(boost::asio::const_buffer buffer, const std::size_t size);

    std::size_t cursor_;
    std::string next_message_size_; // Buffer for receiving a message size when fragmented
    message_size_t next_message_size_bytes_needed_; // Initialized to sizeof(message_size_t)
    message_size_t current_message_bytes_needed_;   // Initialized to 0
    message_size_t current_message_size_;           // Total size of the message we are currently parsing
    flip_flop_reservoir partial_messages_;
    std::vector<boost::asio::const_buffer> received_messages_buffers_;
    rle2::message_parser message_parser_;
};

} // namespace rle2

#endif
