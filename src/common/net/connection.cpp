#include "connection.hpp"
#include "connection_manager_if.hpp"

#include <utility>
#include <vector>

namespace rle2 {
namespace net {

connection::connection(std::shared_ptr<boost::asio::ip::tcp::socket> socket,
                       std::shared_ptr<connection_manager_if> manager,
                       std::unique_ptr<read_completion_handler_if> read_completion_handler)
    : socket_(socket),
      connection_manager_(manager),
      read_completion_handler_(std::move(read_completion_handler)),
      underlaying_buffer_()
{
}

void connection::set_read_handler(std::unique_ptr<read_completion_handler_if> read_handler)
{
    read_completion_handler_ = std::move(read_handler);
}

void connection::start()
{
    read();
}

void connection::stop()
{
    socket_->close();
}

void connection::write(const std::vector<boost::asio::const_buffer>& buffers)
{
    // Asynchronously send buffers through the socket
    auto self(shared_from_this());
    boost::asio::async_write(*socket_, buffers, [this, self](boost::system::error_code ec, std::size_t) {
        if (!ec)
        {
           // TODO: Should we do anything here?
        }

        if (ec != boost::asio::error::operation_aborted)
        {
            if (connection_manager_)
            {
                connection_manager_->stop(self);
            }
        }
    });
}

void connection::shutdown()
{
    boost::system::error_code ignored_ec;
    socket_->shutdown(boost::asio::ip::tcp::socket::shutdown_both, ignored_ec);
}

void connection::read()
{
    auto self(shared_from_this());
    boost::asio::async_read(*socket_, boost::asio::buffer(underlaying_buffer_),
        [this, self](boost::system::error_code ec, std::size_t bytes_transferred) {
        if (!ec)
        {
            if (read_completion_handler_)
            {
                read_completion_handler_->parse(boost::asio::buffer(underlaying_buffer_), bytes_transferred);
            }
            read(); // Perform the next async read so we give work to the io_service.
        }
        else if (ec != boost::asio::error::operation_aborted)
        {
            if (connection_manager_)
            {
                connection_manager_->stop(self);
            }
        }
    });
}

} // namespace net
} // namespace rle2
