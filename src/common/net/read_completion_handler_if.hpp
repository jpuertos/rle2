#ifndef RLE2_NET_READ_COMPLETION_HANDLER_IF_HPP
#define RLE2_NET_READ_COMPLETION_HANDLER_IF_HPP

#include <boost/asio.hpp>
#include <vector>

namespace rle2 {
namespace net {

class read_completion_handler_if
{
public:
    virtual void parse(boost::asio::const_buffer buffer, const std::size_t size) = 0;
};

} // namespace net
} // namespace rle2

#endif
