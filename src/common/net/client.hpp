#ifndef RLE2_NET_CLIENT_HPP
#define RLE2_NET_CLIENT_HPP

#include "connection.hpp"
#include "connection_manager_if.hpp"

#include <boost/asio.hpp>
#include <memory>

namespace rle2 {
namespace net {

class client
{
public:
    client(std::shared_ptr<boost::asio::io_service> io_service, std::shared_ptr<connection_manager_if> connection_manager);
    client(const client&) = delete;
    client& operator=(const client&) = delete;
    ~client() = default;

    void connect(const std::string& address, const std::string& port);
    void send(std::vector<boost::asio::const_buffer>& data);
    void run();

private:
    void handle_resolve(const boost::system::error_code& err, boost::asio::ip::tcp::resolver::iterator endpoint_iterator);

    void handle_connect(const boost::system::error_code& err, std::shared_ptr<boost::asio::ip::tcp::socket> socket);

    std::shared_ptr<boost::asio::io_service> io_service_;
    std::shared_ptr<connection_manager_if> connection_manager_;
    boost::asio::ip::tcp::resolver resolver_;
};

} // namespace net
} // namespace rle2

#endif
