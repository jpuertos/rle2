#include "client.hpp"
#include <boost/bind.hpp>

#include <iostream> // TODO: This is because we are not properly logging yet

namespace rle2 {
namespace net {

client::client(std::shared_ptr<boost::asio::io_service> io_service, std::shared_ptr<connection_manager_if> connection_manager)
    : io_service_(io_service),
      connection_manager_(connection_manager),
      resolver_(*io_service)
{
}

void client::connect(const std::string& address, const std::string& port)
{
    boost::asio::ip::tcp::resolver::query query(address, port);
    resolver_.async_resolve(
        query, boost::bind(&client::handle_resolve, this, boost::asio::placeholders::error, boost::asio::placeholders::iterator));
}

void client::send(std::vector<boost::asio::const_buffer>& data)
{
    // Get connection.
    // And simply call write...
}

void client::run()
{
    io_service_->run();
}

void client::handle_resolve(const boost::system::error_code& err, boost::asio::ip::tcp::resolver::iterator endpoint_iterator)
{
    if (!err)
    {
        // TODO: Log resolve success.
        auto socket = std::make_shared<boost::asio::ip::tcp::socket>(*io_service_); // TODO: Try to use a unique_ptr of socket.

        // Attempt a connection to each endpoint in the list until we
        // successfully establish a connection.
        boost::asio::async_connect(*socket, endpoint_iterator,
                                   boost::bind(&client::handle_connect, this, boost::asio::placeholders::error, socket));
    }
    else
    {
        // TODO: Use logger
        std::cout << "Error: " << err.message() << "\n";
    }
}

void client::handle_connect(const boost::system::error_code& err, std::shared_ptr<boost::asio::ip::tcp::socket> socket)
{
    if (!err)
    {
        // The connection was successful. Create the connection.
        // TODO: Log
        auto c = std::make_shared<connection>(socket, connection_manager_, nullptr);
        connection_manager_->start(c);
    }
    else
    {
        // TODO: Log, use logger
        //std::cout << "Error: " << err.message() << "\n";
    }
}

} // namespace net
} // namespace rle2
