#ifndef RLE2_NET_SERVER_HPP
#define RLE2_NET_SERVER_HPP

#include "connection_manager_if.hpp"

#include <boost/asio.hpp>
#include <string>

namespace rle2 {
namespace net {

class server
{
public:
    server(const server&) = delete;
    server& operator=(const server&) = delete;
    ~server() = default;

    /// Construct the server to listen on the specified TCP address and port.
    explicit server(std::shared_ptr<boost::asio::io_service> io_service,
                    const std::string& address,
                    const std::string& port,
                    std::shared_ptr<connection_manager_if> connection_manager);

    /// Run the server's io_service loop.
    void run();

private:
    /// Perform an asynchronous accept operation.
    void accept();

    /// Wait for a request to stop the server.
    void await_stop();

    /// The io_service used to perform asynchronous operations.
    std::shared_ptr<boost::asio::io_service> io_service_;

    /// The signal_set is used to register for process termination notifications.
    boost::asio::signal_set signals_;

    /// Acceptor used to listen for incoming connections.
    boost::asio::ip::tcp::acceptor acceptor_;

    /// The connection manager which owns all live connections.
    std::shared_ptr<connection_manager_if> connection_manager_;
};

} // namespace net
} // namespace rle2

#endif // RLE2_SERVER_HPP
