#ifndef RLE2_NET_CONNECTION_HPP
#define RLE2_NET_CONNECTION_HPP

#include "read_completion_handler_if.hpp"

#include <array>
#include <boost/asio.hpp>
#include <functional>
#include <memory>
#include <vector>

namespace rle2 {
namespace net {

class connection_manager_if;

auto constexpr RECEIVE_BUFFER_SIZE = 8 * 1024; // 8KB Buffer, what happens if the received data is bigger that this? (case of sending chunks of map)

/// Represents a single connection from a client.
class connection : public std::enable_shared_from_this<connection>
{
public:
    using underlaying_buffer_t = std::array<std::uint8_t, RECEIVE_BUFFER_SIZE>;

    /// Construct a connection with the given socket.
    explicit connection(std::shared_ptr<boost::asio::ip::tcp::socket> socket,
                        std::shared_ptr<connection_manager_if> manager,
                        std::unique_ptr<read_completion_handler_if> read_completion_handler);

    connection(const connection&) = delete;
    connection& operator=(const connection&) = delete;
    ~connection() = default;

    /// Sets the read completion handler
    void set_read_handler(std::unique_ptr<read_completion_handler_if> read_handler);

    /// Start the first asynchronous operation for the connection.
    void start();

    /// Stop all asynchronous operations associated with the connection.
    void stop();

    /// Asynchronously writes a stream of messages
    void write(const std::vector<boost::asio::const_buffer>& buffers);

    void shutdown();

private:
    /// Performs asynch read
    void read();

    /// Socket for the connection.
    std::shared_ptr<boost::asio::ip::tcp::socket> socket_;

    /// The manager for this connection.
    std::shared_ptr<connection_manager_if> connection_manager_;

    /// The handler used to process the incoming messages.
    std::unique_ptr<read_completion_handler_if> read_completion_handler_;

    /// Buffer for incoming data.
    underlaying_buffer_t underlaying_buffer_;
};

} // namespace net
} // namespace rle2

#endif // RLE2_CONNECTION_HPP
