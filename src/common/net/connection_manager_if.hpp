#ifndef RLE2_NET_CONNECTION_MANAGER_IF_HPP
#define RLE2_NET_CONNECTION_MANAGER_IF_HPP

#include <memory>

namespace rle2 {
namespace net {

class connection;

/**
 * @brief The connection_manager_if class describes basic functionality that
 * a connection manager needs to implement.
 */
class connection_manager_if
{
public:
    /**
     * @brief add_connection Adds and starts the specified connection
     * @param c Connection
     */
    virtual void start(std::shared_ptr<connection> c) = 0;

    /**
     * @brief stop Stops and removes a connection
     * @param c Connection
     */
    virtual void stop(std::shared_ptr<connection> c) = 0;
    /**
     * @brief stop_all Stops all the connections
     */
    virtual void stop_all() = 0;
};

} // namespace net
} // namespace rle2

#endif
