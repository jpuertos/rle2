#include "server.hpp"
#include "connection.hpp"

#include <signal.h>
#include <utility>

namespace rle2 {
namespace net {

server::server(std::shared_ptr<boost::asio::io_service> io_service,
               const std::string& address,
               const std::string& port,
               std::shared_ptr<connection_manager_if> connection_manager)
    : io_service_(io_service),
      signals_(*io_service_),
      acceptor_(*io_service_),
      connection_manager_(connection_manager)
{
    // Register to handle the signals that indicate when the server should exit.
    // It is safe to register for the same signal multiple times in a program,
    // provided all registration for the specified signal is made through Asio.
    signals_.add(SIGINT);
    signals_.add(SIGTERM);
#if defined(SIGQUIT)
    signals_.add(SIGQUIT);
#endif // defined(SIGQUIT)

    await_stop();

    // Open the acceptor with the option to reuse the address (i.e. SO_REUSEADDR).
    boost::asio::ip::tcp::resolver resolver(*io_service_);
    boost::asio::ip::tcp::resolver::query query(address, port);
    boost::asio::ip::tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
    boost::asio::ip::tcp::endpoint endpoint = *endpoint_iterator;
    acceptor_.open(endpoint.protocol());
    acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
    acceptor_.bind(endpoint);
    acceptor_.listen();

    accept();
}

void server::run()
{
    // The io_service::run() call will block until all asynchronous operations
    // have finished. While the server is running, there is always at least one
    // asynchronous operation outstanding: the asynchronous accept call waiting
    // for new incoming connections.
    io_service_->run();
}

void server::accept()
{
    auto socket = std::make_shared<boost::asio::ip::tcp::socket>(*io_service_); // TODO: Try to use a unique_ptr of socket.
    acceptor_.async_accept(*socket, [this, socket](boost::system::error_code ec) mutable {
        // Check whether the server was stopped by a signal before this
        // completion handler had a chance to run.
        if (!acceptor_.is_open())
        {
            return;
        }

        if (!ec)
        {
            // Don't set a read handler yet to avoid introducing dependencies on the handler type.
            // conection_managet will set the read handler befor starting the connection.
            auto c = std::make_shared<connection>(socket, connection_manager_, nullptr);
            connection_manager_->start(c);
        }

        // TODO: Log errors if any.

        accept();
    });
}

void server::await_stop()
{
    signals_.async_wait([this](boost::system::error_code /*ec*/, int /*signo*/) {
        // The server is stopped by cancelling all outstanding asynchronous
        // operations. Once all operations have finished the io_service::run()
        // call will exit.
        acceptor_.close();
        connection_manager_->stop_all();
    });
}

} // namespace net
} // namespace rle2
