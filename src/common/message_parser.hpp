#ifndef RLE2_MESSAGE_PARSER_HPP
#define RLE2_MESSAGE_PARSER_HPP

#include "common/queue.hpp"
#include "common/pb/wrapper_message.pb.h"

#include <boost/asio.hpp>
#include <memory>

namespace rle2
{

class message_parser final
{
public:
    message_parser(std::shared_ptr<rle2::queue<rle2::pb::wrapper_message>> queue);
    ~message_parser() = default;
    message_parser(const message_parser&) = delete;
    message_parser& operator=(const message_parser&) = delete;
    message_parser(message_parser&&) = delete;
    message_parser& operator=(message_parser&&) = delete;

    void parse(const std::vector<boost::asio::const_buffer>& buffers);

private:
    std::shared_ptr<rle2::queue<rle2::pb::wrapper_message>> queue_;
    rle2::pb::wrapper_message wrapped_message_;
};

}

#endif

