#include "client/client_connection_manager.hpp"
#include "common/stream_parser.hpp"

namespace rle2 {
namespace net {

client_connection_manager::client_connection_manager()
{
}

void client_connection_manager::start(std::shared_ptr<connection> c)
{
    queue_ = std::make_shared<rle2::queue<rle2::pb::wrapper_message>>();
    connection_ = c;
    connection_->set_read_handler(std::make_unique<stream_parser>(queue_));
    connection_->start();
}

void client_connection_manager::stop(std::shared_ptr<connection> c)
{
    if(c.get() != connection_.get()) // This should never happen
    {
        // TODO: Log an error.
        assert(false);
    }
    connection_->stop();
}

void client_connection_manager::stop_all()
{
    connection_->stop();
}

} // namespace net
} // namespace rle2
