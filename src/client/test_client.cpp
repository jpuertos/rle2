#include "common/net/client.hpp"
#include "client_connection_manager.hpp"

#include <iostream>
#include <string>

int main(int argc, char* argv[])
{
    try
    {
        // Check command line arguments.
        if (argc != 3)
        {
            std::cerr << "Usage: http_server <address> <port>\n";
            std::cerr << "  For IPv4, try:\n";
            std::cerr << "    server 127.0.0.1 27098 .\n";
            std::cerr << "  For IPv6, try:\n";
            std::cerr << "    receiver 0::0 27098 .\n";
            return 1;
        }

        auto io_service = std::make_shared<boost::asio::io_service>(1); // Why 1, what does it mean?
        auto connection_manager = std::make_shared<rle2::net::client_connection_manager>();

        // Initialise the client.
        rle2::net::client client(io_service, connection_manager);
        client.connect(argv[1], argv[2]);

        // Run the client until stopped.
        client.run();
    }
    catch (std::exception& e)
    {
        std::cerr << "exception: " << e.what() << "\n";
    }

    return 0;
}
