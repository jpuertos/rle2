#ifndef RLE2_CLIENT_CONNECTION_MANAGER_HPP
#define RLE2_CLIENT_CONNECTION_MANAGER_HPP

#include "common/net/connection.hpp"
#include "common/net/connection_manager_if.hpp"
#include "common/queue.hpp"
#include "common/pb/wrapper_message.pb.h"

namespace rle2 {
namespace net {

/**
* @brief The client_connection_manager class manages only one connection
*/
class client_connection_manager : public connection_manager_if
{
public:
    client_connection_manager(const client_connection_manager&) = delete;
    client_connection_manager& operator=(const client_connection_manager&) = delete;
    client_connection_manager();

    virtual void start(std::shared_ptr<connection> c) override;

    virtual void stop(std::shared_ptr<connection> c) override;

    virtual void stop_all() override;

private:
    /// The managed connection.
    std::shared_ptr<connection> connection_;
    std::shared_ptr<rle2::queue<rle2::pb::wrapper_message>> queue_; // For sending the incomming messages to the handler thread
};

} // namespace net
} // namespace rle2

#endif // RLE2_CLIENT_CONNECTION_MANAGER_HPP
